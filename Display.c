#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "Display.h"


// Fonction pour créer une fenêtre de jeu munie d'un renderer SDL
SDL_Renderer * create_win_and_renderer (SDL_Window *window, SDL_Renderer *renderer) {

    // Création de la SDL
    if(0 != SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(stderr, "Erreur SDL_Init : %s\n", SDL_GetError());
        SDL_Quit();
        return NULL;
    }

    // Création d'une fenêtre
    window = SDL_CreateWindow("Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if(window == NULL)
    {
        fprintf(stderr, "Erreur SDL_CreateWindow : %s\n", SDL_GetError());
        SDL_Quit();
        return NULL;
    }

    // Création d'un renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if(renderer == NULL)
    {
        fprintf(stderr, "Erreur SDL_CreateRenderer : %s\n", SDL_GetError());
        if(NULL != window)
            SDL_DestroyWindow(window);
        SDL_Quit();
        return NULL;
    }

    return renderer;


};



// Fonction pour afficher un noeud
int display_node(SDL_Renderer * renderer, int x_pos, int y_pos){

    SDL_Rect rect;
    rect.x = x_pos * NODE_SIZE+1;
    rect.y = y_pos * NODE_SIZE+1;
    rect.h = NODE_SIZE-1;
    rect.w = NODE_SIZE-1;

    //Actions sur la fenêtre
    SDL_RenderFillRect(renderer, &rect);

    return 0;
}


// Fonction pour afficher le plateau de jeu
int display_grid(SDL_Renderer * renderer, int grid[BOARD_HEIGHT][BOARD_WIDTH] ){

    // Effacement du renderer
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);

    // Coloration du plateau de jeu
    for (int x=0; x<BOARD_WIDTH; x++){
        for (int y = 0; y<BOARD_HEIGHT; y++){

            switch (grid[y][x]) {
            case -1 :   // Murs
                SDL_SetRenderDrawColor(renderer, COLOR_R, COLOR_G, COLOR_B, COLOR_A);   // Couleur principale
                display_node(renderer, x, y);
                break;
            case 0 :
                // Si la case est vide, on n'affiche rien
                break;
            case 1 :    // Pomme
                SDL_SetRenderDrawColor(renderer, COLOR_R, COLOR_G, COLOR_B, COLOR_A);   // Couleur principale
                display_node(renderer, x, y);
                break;
            case 2 :    // Serpent 1
                SDL_SetRenderDrawColor(renderer, 255, 255, 50, 0);   // Couleur principale
                display_node(renderer, x, y);
                break;
            case 3 :    // Serpent 2
                SDL_SetRenderDrawColor(renderer, 255, 50, 0, 255);   // Couleur rouge
                display_node(renderer, x, y);
                break;
            case 4 :    // Serpent 3
                SDL_SetRenderDrawColor(renderer, 50, 0, 255, 255);   // Couleur bleue
                display_node(renderer, x, y);
                break;
            default:
                break;
            }
        }
    }

    // Actualisation du renderer
    SDL_RenderPresent(renderer);
    return 0;

}
