#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "params.h"

// Fonction pour créer une fenêtre de jeu munie d'un renderer SDL
SDL_Renderer * create_win_and_renderer (SDL_Window *window, SDL_Renderer *renderer);

// Fonction pour afficher un noeud
int display_node(SDL_Renderer * renderer, int x_pos, int y_pos);

// Fonction pour afficher une pomme (en forme de cercle)
int display_apple(SDL_Renderer * renderer, int x_pos, int y_pos);

// Fonction pour afficher le serpent
int display_grid(SDL_Renderer * renderer, int grid[BOARD_HEIGHT][BOARD_WIDTH] );
