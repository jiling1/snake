#include "Game.h"



// Fonction de création d'un serpent de taille 5
struct Snake * create_snake(struct Game *game, int id_snake){

    // Position de la tête du serpent
    int snake_head_x = (BOARD_WIDTH - (LEFT_WALL_WIDTH + RIGHT_WALL_WIDTH))/2 + 10 * (id_snake - 1);
    int snake_head_y = (BOARD_HEIGHT - (TOP_WALL_WIDTH + DOWN_WALL_WIDTH))/2;

    // Noeud 1 (tête)
    struct Node *head = malloc(sizeof(struct Node));
    head->next = NULL;
    head->x_pos = snake_head_x ;
    head->y_pos = snake_head_y;

    // Noeud 2
    struct Node *node1 = malloc(sizeof(struct Node));
    node1->next = head;
    node1->x_pos = snake_head_x ;
    node1->y_pos = snake_head_y + 1;

    // Noeud 3
    struct Node *node2 = malloc(sizeof(struct Node));
    node2->next = node1;
    node2->x_pos = snake_head_x;
    node2->y_pos = snake_head_y + 2;

    // Noeud 4
    struct Node *node3 = malloc(sizeof(struct Node));
    node3->next = node2;
    node3->x_pos = snake_head_x;
    node3->y_pos = snake_head_y + 3;

    // Noeud 5 (queue)
    struct Node *tail = malloc(sizeof(struct Node));
    tail->next = node3;
    tail->x_pos = snake_head_x;
    tail->y_pos = snake_head_y + 4;

    // Serpent
    struct Snake *snake = malloc(sizeof(struct Snake));
    snake->head = head;
    snake->tail = tail;
    snake->snake_dir = UP;
    snake->length = 5;

    // Ajout du serpent à la grille de jeu
    for (int n=0; n<5; n++) {
        game->grid[snake_head_y + n][snake_head_x] = 2 + id_snake;
    }

    return snake;
}


//Fonction d'initialisation de partie
int game_init(struct Game *game, int nb_players) {

    // Ajout du nombre de joueurs
    game->nb_players = nb_players;

    // INITIALISATION DE LA GRILLE
    // Murs horizontaux
    printf("Generation des murs horizontaux\n");
    for (int x=0 ; x < BOARD_WIDTH ; x++)
    {
        game->grid[0][x] = -1;
        game->grid[BOARD_HEIGHT-1][x] = -1;
    }

    // Murs verticaux
    printf("Generation des murs verticaux\n");
    for (int y=0 ; y < BOARD_HEIGHT ; y++)
    {
        game->grid[y][0] = -1;
        game->grid[y][BOARD_WIDTH-1] = -1;
    }

    // Terrain de jeu
    printf("Generation du terrain de jeu\n");
    for (int x=1 ; x < BOARD_WIDTH - 1; x++)
    {
        for (int y=1 ; y< BOARD_HEIGHT - 1; y++)
        {
            game->grid[y][x] = 0;
        }
    }

    // Pomme
    generate_new_apple(game);

    // Serpents
    printf("Generation des serpent\n");
    for (int i = 0; i < game->nb_players; i++){
        struct Snake *snake = create_snake(game, i);
        game->snakes[i] = snake;
    }

}


//Fonction qui détermine si le serpent doit grandir
int should_grow(struct Game *game, int id_snake)
{
    // On regarde si la prochaine case dans la direction du serpent est la case de la pomme
    switch(game->snakes[id_snake]->snake_dir){
        case LEFT:
            return (game->apple->x_pos == game->snakes[id_snake]->head->x_pos - 1);

        case UP:
            return (game->apple->y_pos == game->snakes[id_snake]->head->y_pos - 1);

        case RIGHT:
            return (game->apple->x_pos == game->snakes[id_snake]->head->x_pos + 1);

        case DOWN:
            return (game->apple->y_pos == game->snakes[id_snake]->head->y_pos + 1);
        default:
            return 0;
    }
}

// Fonction qui crée une nouvelle pomme
void generate_new_apple(struct Game *game)
{
    printf("Generation d'une pomme\n");
    struct Node *new_apple = malloc(sizeof(struct Node));
    int apple_x = 0 , apple_y = 0;
    while(game->grid[apple_y][apple_x] != 0) {  // On cherche une case vide pour y mettre la pomme
        apple_x = 1 + rand() % (BOARD_WIDTH - 2);
        apple_y = 1 + rand() % (BOARD_HEIGHT - 2);
    }

    // Quand on a trouvé une case vide, on y place la pomme
    new_apple->x_pos=apple_x;
    new_apple->y_pos=apple_y;
    new_apple->next=NULL;

    game->grid[apple_y][apple_x] = 1; //Ajout de la pomme au grid
    game->apple = new_apple;          //Ajout de la pomme au Game
}

//Fonction qui vérifie une collision avec un mur ou le corps d'un serpent
//Retourne 0 si le deplacement est faisable, -1 sinon.
int check_collisions(struct Game *game, int id_snake) {
    switch(game->snakes[id_snake]->snake_dir){
        case LEFT: // Le serpent va vers la gauche, on verifie si la cellule gauche est vide.
            if(game->grid[game->snakes[id_snake]->head->y_pos][game->snakes[id_snake]->head->x_pos - 1] != 0) {
                return -1;
            }
            return 0;
            break;
        case RIGHT: // Le serpent va vers la droite, on verifie si la cellule droite est vide.
            if(game->grid[game->snakes[id_snake]->head->y_pos][game->snakes[id_snake]->head->x_pos + 1] != 0) {
                return -1;
            }
            return 0;
            break;
        case UP: // Le serpent va vers le haut, on verifie si la cellule haute est vide.
            if(game->grid[game->snakes[id_snake]->head->y_pos - 1][game->snakes[id_snake]->head->x_pos] != 0) {
                return -1;
            }
            return 0;
            break;
        case DOWN: // Le serpent va vers le bas, on verifie si la cellule basse est vide.
            if(game->grid[game->snakes[id_snake]->head->y_pos + 1][game->snakes[id_snake]->head->x_pos] != 0) {
                return -1;
            }
            return 0;
            break;
        default:
            return -2;
            break;
    }
}

// Fonction de mouvement principale du serpent
int move(struct Game *game, int id_snake) {

    if(check_collisions(game, id_snake) == 0) { // Si la prochaine case dans la direction du serpent est valide
        game->grid[game->snakes[id_snake]->tail->y_pos][game->snakes[id_snake]->tail->x_pos] = 0;   // On efface la queue du serpent de la grille de jeu
        snk_move(game->snakes[id_snake]);   // On fait avancer le serpent
        game->grid[game->snakes[id_snake]->head->y_pos][game->snakes[id_snake]->head->x_pos] = 2 + id_snake; // On ajoute la nouvelle tête du serpent à la grille de jeu
        return 0;
    }
    return -1;
}

// Fonction de croissance principale d'un serpent
int grow(struct Game *game, int id_snake) {
    if((check_collisions(game, id_snake) != 0) && (should_grow(game, id_snake) != 0)) { // Si la prochaine case dans la direction du serpent est la pomme
        snk_grow(game->snakes[id_snake]);   // On fait grandir le serpent
        game->grid[game->snakes[id_snake]->head->y_pos][game->snakes[id_snake]->head->x_pos] = 2 + id_snake; // On ajoute la nouvelle tête du serpent à la grille de jeu
        generate_new_apple(game);   // On crée une nouvelle pomme
        return 0;
    }
    return -1;
}


// Fonction de réinitialisation de la partie
void reset(struct Game *game) {
    game_init(game, game->nb_players);
}
