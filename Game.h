#include <stdio.h>
#include <stdlib.h>
#include "params.h"
#include "Snake.h"

// Structure d'une partie de Snake
struct Game {
    int nb_players; // Nombre de joueurs
    struct Snake *snakes[NB_MAX_PLAYERS]; // Liste des serpents
    struct Node *apple; // Pomme
    int grid[BOARD_HEIGHT][BOARD_WIDTH]; // Plateau de jeu => -1:mur, 0:vide, 1:pomme, 2-3-4:serpents
};

/////////////// FONCTION DE GESTION D'UNE PARTIE ///////////////

// Fonction d'initialisation d'une partie
int game_init(struct Game *game, int nb_players) ;


/////////////// FONCTIONS DE GESTION DE LA LOGIQUE ///////////////

//Fonction qui détermine si le serpent doit grandir
int should_grow(struct Game *game, int id_snake);

// Fonction qui crée une nouvelle pomme
void generate_new_apple(struct Game *game);

//Fonction qui vérifie une collision avec un mur ou le corps du serpent
int check_collisions(struct Game *game, int id_snake);

//Fonction principale de deplacement
int move(struct Game *game, int id_snake);

//Fonction principale pour grandir
int grow(struct Game *game, int id_snake);

//Fonction de reset
void reset(struct Game *game);
