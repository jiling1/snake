#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <pthread.h>

#include "Display.h"
#include "params.h"
#include "Game.h"
#include "Play_game.h"



// Définition des variables globales
struct Game *game;
int in_game[NB_MAX_PLAYERS];

int new_dir[3] = {0, 0, 0};
int run = 1;

int tempsPrecedent = 0, tempsActuel = 0;

// Création fenêtre SDL
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;



// Fonction qui filtre les appuis successifs sur une même touche, pour ne pas les ajouter à la file des évènements
int keyFilter(void* userdata, SDL_Event* event){
    if (event->type == SDL_KEYDOWN){
        if (event->key.keysym.sym == SDLK_UP && new_dir[0] == 0 ||
            event->key.keysym.sym == SDLK_DOWN && new_dir[0] == 2 ||
            event->key.keysym.sym == SDLK_RIGHT && new_dir[0] == 1 ||
            event->key.keysym.sym == SDLK_LEFT && new_dir[0] == 3 ||

            event->key.keysym.sym == SDLK_z && new_dir[1] == 0 ||
            event->key.keysym.sym == SDLK_s && new_dir[1] == 2 ||
            event->key.keysym.sym == SDLK_d && new_dir[1] == 1 ||
            event->key.keysym.sym == SDLK_q && new_dir[1] == 3 ||

            event->key.keysym.sym == SDLK_o && new_dir[2] == 0 ||
            event->key.keysym.sym == SDLK_l && new_dir[2] == 2 ||
            event->key.keysym.sym == SDLK_m && new_dir[2] == 1 ||
            event->key.keysym.sym == SDLK_k && new_dir[2] == 3)
            return 0;
        else
            return 1;
    }
}


void *snk_thread_handler(void* arg){

    int *event = arg;  // event récupère key_snk

    // On traite le nouvel évènement 
    switch(event[1])
    {
        case 4: // Touche R -> on réinitialise le jeu pour tous les joueurs
            reset(game);
            for (int i = 0; i < game->nb_players; i++){
                in_game[i] = 1;
            }
            break;
        case 0: // Flèche haut
            if (game->snakes[event[0]]->snake_dir != DOWN) game->snakes[event[0]]->snake_dir = UP;
            break;
        case 2: // Flèche bas
            if (game->snakes[event[0]]->snake_dir != UP) game->snakes[event[0]]->snake_dir = DOWN;
            break;
        case 1: // Flèche droite
            if (game->snakes[event[0]]->snake_dir != LEFT) game->snakes[event[0]]->snake_dir = RIGHT;
            break;
        case 3: // Flèche gauche
            if (game->snakes[event[0]]->snake_dir != RIGHT) game->snakes[event[0]]->snake_dir = LEFT;
            break;
        default: // Toutes les touches pour quitter
            run = 0;
            break;

    }

    // Partie faisant avancer le serpent en question
    if(in_game[event[0]]){  // Si il est toujours en jeu
        if (grow(game, event[0]) != 0) {; // Si le serpent ne peut pas grandir
            if(move(game, event[0]) != 0){ // Si le serpent entre en collision, fin de la partie.
                printf("Fin de la partie pour le serpent %d !\n\n", event[0]);
                in_game[event[0]] = 0;
            }
        }
    }

}





// Fonction de jeu
void play_game(void){

    SDL_Event event;

    int new_board[BOARD_HEIGHT][BOARD_WIDTH];
    int rep_err;

    // Création de l'image SDL
    renderer = create_win_and_renderer(window, renderer);

    // Ajout du filtre anti appuis successifs
    SDL_SetEventFilter(keyFilter, NULL);

    // Création des threads serpent
    pthread_t thread_snakes[game->nb_players];

    // BOUCLE DE JEU
    while (run) {

        //Traitement des entrées clavier, 1 seule par tour de boucle
        SDL_PollEvent(&event);

        // Tri des appuis sur une touche du clavier -> SEULEMENT 3 JOUEURS MAX POSSIBLES
        switch(event.type)
        {
            case SDL_QUIT:
                run = 0;
                new_dir[0] = -1;
                break;
            case SDL_WINDOWEVENT_CLOSE :
                run = 0;
                new_dir[0] = -1;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_ESCAPE: /* Appui sur la touche Echap, on arrête le programme */
                        run = 0;
                        new_dir[0] = -1;
                        break;

                    case SDLK_UP: // Flèche haut J1
                        new_dir[0] = 0;
                        break;
                    case SDLK_DOWN: // Flèche bas J1
                        new_dir[0] = 2;
                        break;
                    case SDLK_RIGHT: // Flèche droite J1
                        new_dir[0] = 1;
                        break;
                    case SDLK_LEFT: // Flèche gauche J1
                        new_dir[0] = 3;
                        break;

                    case SDLK_z: // Flèche haut J2
                        new_dir[1] = 0;
                        break;
                    case SDLK_s: // Flèche bas J2
                        new_dir[1] = 2;
                        break;
                    case SDLK_d: // Flèche droite J2
                        new_dir[1] = 1;
                        break;
                    case SDLK_q: // Flèche gauche J2
                        new_dir[1] = 3;
                        break;

                    case SDLK_o: // Flèche haut J3
                        new_dir[2] = 0;
                        break;
                    case SDLK_l: // Flèche bas J3
                        new_dir[2] = 2;
                        break;
                    case SDLK_m: // Flèche droite J3
                        new_dir[2] = 1;
                        break;
                    case SDLK_k: // Flèche gauche J3
                        new_dir[2] = 3;
                        break;

                    case SDLK_r: // Touche R
                        new_dir[0] = 4;
                        break;
                }
                break;
        }

        /* Condition qui permet d'envoyer les entrées claviers à intervalles réguliers,
        et donc de faire beaucoup de tours de boucle avant d'envoyer,
        ce qui permet d'être sur d'avoir récupéré les entrées des 3 joueurs */
        tempsActuel = SDL_GetTicks();
        if (tempsActuel - tempsPrecedent > 150)
        {
            tempsPrecedent = tempsActuel;  /* Le temps "actuel" devient le temps "precedent" pour nos futurs calculs */

            // Création successive des threads de mouvement des serpents
            for (int i = 0; i < game->nb_players; i++){

                int key_snk[2] = {i, new_dir[i]};

                // On crée le thread traitant le mouvement
                if (pthread_create(&thread_snakes[i], NULL, snk_thread_handler, key_snk) == -1 ){  // Appel de la fonction thread handler
                    perror("Erreur de création du thread\n");
                    run = 0;}
            }

            // On attend la fin du thread
            for (int i = 0; i < game->nb_players; i++) {
                pthread_join (thread_snakes[i], NULL);
            }

            display_grid(renderer, game->grid);

            // Si on a recommencé une partie, on réinitialise la direction de tous les serpents à celle de départ
            if (new_dir[0] == 4) {
                new_dir[0] = 0;
                new_dir[1] = 0;
                new_dir[2] = 0;
            }
        }
    }
}



// Thread principal
int main(int argc, char** argv)
{

    printf("\nBonjour et bienvenue dans ce jeu de Snake !\nCombien de joueurs etes-vous ? (1/2/3) : ");

    int nb_players;
    scanf("%d", &nb_players);

    if (nb_players == 1 || nb_players == 2 || nb_players == 3) {
        printf("\nC'est parti ! \n");

        printf("Debut de la partie\n");

        // Création de la partie
        game = malloc(sizeof(struct Game));
        game_init(game, nb_players);

        // Mise en jeu des serpents
        for (int i = 0; i < nb_players; i++){
            in_game[i] = 1;
        }
        // LANCEMENT DE LA PARTIE
        play_game();
    }

    // On libère l'espace
    for (int i = 0; i < nb_players; i++){
        struct Node * node = game->snakes[i]->tail;
        for (int i = 0; i<game->snakes[i]->length; i++){
            game->snakes[i]->tail = game->snakes[i]->tail->next;
            free(node);
            node = game->snakes[i]->tail;
        }
        free(game->snakes[i]);
    }
    free(game->apple);
    free(game);

    if(NULL != renderer) SDL_DestroyRenderer(renderer);
    if(NULL != window) SDL_DestroyWindow(window);
    SDL_Quit();

    printf("\nAu revoir !\n");

    return 0;

}
