#include <stdio.h>
#include <stdlib.h>
#include "Snake.h"

// Fonction "avancer"
int snk_move(struct Snake *snake)
{
    struct Node * oldtail = snake->tail;
    snk_grow(snake);                    // On agrandit le serpent d'une case dans la direction snake->dir
    snake->length -= 1;
    snake->tail = snake->tail->next;    // Puis on retire le dernier noeud (le serpent a donc avancé d'une case)
    free(oldtail);

}

// Fonction "grandir"
int snk_grow(struct Snake *snake)
{   
    struct Node *new_node = malloc(sizeof(struct Node));    // On crée le noeud sur lequel la tête du serpent arrive, en initialisant sa position au même endroit que la tête du serpent

    new_node->next = 0;
    new_node->y_pos = snake->head->y_pos;
    new_node->x_pos = snake->head->x_pos;
    
    switch (snake->snake_dir) {                   // On décale d'une case dans la direction souhaitée le nouveau noeud, pour qu'il prolonge le serpent 
        case LEFT:                              
            new_node->x_pos -= 1;
            break;

        case UP:
            new_node->y_pos -= 1;
            break;

        case RIGHT:
            new_node->x_pos += 1;
            break;

        case DOWN:
            new_node->y_pos += 1;
            break;
    };

    struct Node *oldhead = snake->head;
    snake->head = new_node;
    oldhead->next = new_node;              // On ajoute le nouveau noeud au serpent
    snake->length += 1;                        // On incrémente sa longueur
}