
// Enumération des directions possibles
enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
};

// Structure d'un noeud du corps du serpent
// le pointeur next pointe vers le noeud le plus proche de la tête du serpent (le premier élément de la liste est donc la queue)
struct Node {
    struct Node *next;  // Pointeur vers le prochain noeud du corps du serpent (vers la tête)
    int x_pos;  // Coordonée X du noeud (en nb de noeuds)
    int y_pos;  // Coordonée Y du noeud (en nb de noeuds)
};

// Structure Serpent
struct Snake {
    struct Node *head;  // Pointeur vers la queue du serpent
    struct Node *tail;  // Pointeur vers la tête du serpent
    enum Direction snake_dir;   // Direction du serpent
    int length;   // Longueur du serpent
};

/////////////// FONCTIONS ASSOCIEES A LA STRUCTURE SERPENT ///////////////

// Fonction "avancer"
int snk_move(struct Snake *snake);

// Fonction "grandir"
int snk_grow(struct Snake *snake);
