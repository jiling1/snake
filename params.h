#ifndef PARAMS_H
#define PARAMS_H
///////////////////////// PARAMETRES DE JEU /////////////////////////

// Taille de la fenêtre du programme (en px)
#define WINDOW_WIDTH (BOARD_WIDTH * NODE_SIZE)
#define WINDOW_HEIGHT (BOARD_HEIGHT * NODE_SIZE)

// Taille du plateau de jeu (murs compris) (en nombre de noeuds)
#define BOARD_WIDTH 62
#define BOARD_HEIGHT 52

// Epaisseur des murs (en noeuds)
#define LEFT_WALL_WIDTH 1
#define RIGHT_WALL_WIDTH 1
#define TOP_WALL_WIDTH 1
#define DOWN_WALL_WIDTH 1

// Taille d'un noeud (en px)
#define NODE_SIZE 15

// Vitesses du jeu
#define V_SLOW_SPEED 400
#define SLOW_SPEED 200
#define FAST_SPEED 1000
#define V_FAST_SPEED 50

// Couleur des éléments principaux du jeu
#define COLOR_R 125
#define COLOR_G 246
#define COLOR_B 155
#define COLOR_A 10

// Nombre de joueurs maximum
#define NB_MAX_PLAYERS 3

///////////////////////// PARAMETRES DE CONNEXION /////////////////////////
#define SERVER_IP "127.0.0.1"  // Adresse localhost (interface interne)
#define SERVER_PORT 4000
#define REPLY_SIZE 2000
#define CMD_SIZE 100

///////////////////////// FIN DES PARAMETRES /////////////////////////
#endif
